package operations;

/**
 * This class is used to calculate the sum of 2 given numbers
 * and display a message to keep track what operation is executed
 */
public class AddOperation implements IOperation {

    /**
     * This method will be used to add two doubles.
     *
     * @param n1 a value to be added
     * @param n2 the other value to be added
     * @return the sum of the two doubles
     */
    @Override
    public double calculateResult(double n1, double n2) {
        return n1 + n2;
    }

    @Override
    public String showPreResultMessage() {
        return "The result of addition is = ";
    }
}
