package operations;

/**
 * This class is used to calculate the multiplication of 2 given numbers
 * and display a message to keep track what operation is executed
 */
public class MultiplyOperation implements IOperation {
    @Override
    public double calculateResult(double n1, double n2) {
        return n1 * n2;
    }

    @Override
    public String showPreResultMessage() {
        return "The result of multiplication is = ";
    }
}
