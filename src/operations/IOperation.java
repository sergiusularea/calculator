package operations;


public interface IOperation {
    /**
     * This method will be used to calculate a result
     * by a certain implementation
     */
    double calculateResult(double n1, double n2);

    /**
     * This method will be used to display a message
     * for a certain operation
     *
     * @return a message
     */
    String showPreResultMessage();
}
