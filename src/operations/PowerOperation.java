package operations;

/**
 * This class is used to calculate the raising to the power of 2 given numbers
 * and display a message to keep track what operation is executed
 */
public class PowerOperation implements IOperation {
    @Override
    public double calculateResult(double n1, double n2) {
        int pow = (int) n2;
        double result = 1;
        if (pow == 0) {
            return 1;
        } else {
            for (int i = 1; i <= pow; i++) {
                result *= n1;
            }
        }
        return result;
    }

    @Override
    public String showPreResultMessage() {
        return "The result of raising to the power is = ";
    }
}
