import operations.*;
import readdata.IReadFromConsole;
import readdata.ReadNumber;
import readdata.ReadOperation;
import readdata.ReadYesOrNo;

public class Main {

    public static void main(String[] args) {
        IOperation iOperation = null;
        IReadFromConsole iReadFromConsole;
        System.out.println("Console calculator !");
        while (true) {
            iReadFromConsole = new ReadNumber();
            double firstNumber = Double.parseDouble(iReadFromConsole.read("Insert the first number : "));
            double secondNumber = Double.parseDouble(iReadFromConsole.read("Insert the second number : "));
            iReadFromConsole = new ReadOperation();
            String operation = iReadFromConsole.read("Choose the operation you want : (+ - / * ^) ");
            switch (operation) {
                case "+":
                    iOperation = new AddOperation();
                    break;
                case "-":
                    iOperation = new SubtractOperation();
                    break;
                case "/":
                    if (secondNumber == 0) {
                        System.out.println("can not divide by 0");
                        break;
                    }
                    iOperation = new DivideOperation();
                    break;
                case "*":
                    iOperation = new MultiplyOperation();
                    break;
                case "^":
                    iOperation = new PowerOperation();
                    break;
            }
            assert iOperation != null;
            System.out.println(iOperation.showPreResultMessage() + iOperation.calculateResult(firstNumber, secondNumber));
            iReadFromConsole = new ReadYesOrNo();
            String choice = iReadFromConsole.read("Would you like to continue ? : Yes/No");
            if (choice.toLowerCase().equals("no")) {
                System.out.println("Exitting...");
                return;
            }
        }
    }
}