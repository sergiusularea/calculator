package readdata;

import validations.DoubleNumberValidation;

/**
 * This class is executed until we insert a valid double number
 */
public class ReadNumber implements IReadFromConsole {
    @Override
    public String read(String message) {
        DoubleNumberValidation validation = new DoubleNumberValidation();
        ReadString readString = new ReadString();
        double result;
        while (true) {
            String number = readString.read(message);
            if (validation.validateInput(number)) {
                result = Double.parseDouble(number);
                break;
            }
        }
        return Double.toString(result);
    }
}
