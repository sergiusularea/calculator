package readdata;

import validations.YesOrNoValidation;

/**
 * This class is executed until we insert a valid choice yes/no
 */
public class ReadYesOrNo implements IReadFromConsole {
    @Override
    public String read(String message) {
        YesOrNoValidation validation = new YesOrNoValidation();
        ReadString readString = new ReadString();
        String result;
        do {
            result = readString.read(message);
        } while (!validation.validateInput(result));
        return result;
    }
}
