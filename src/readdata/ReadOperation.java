package readdata;

import validations.OperationValidation;

/**
 * This class is executed until we insert a valid operation
 */
public class ReadOperation implements IReadFromConsole {
    @Override
    public String read(String message) {
        OperationValidation validation = new OperationValidation();
        ReadString readString = new ReadString();
        String result;
        do {
            result = readString.read(message);
        } while (!validation.validateInput(result));
        return result;
    }
}
