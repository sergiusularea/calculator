package validations;

/**
 * This class is used to check the yes/no choice
 */
public class YesOrNoValidation implements IValidation {
    @Override
    public boolean validateInput(String input) {
        String choice = input.toLowerCase();
        String check = null;
        boolean result;
        if (choice.equals("no")) {
            result = true;
            check = "false";
        } else if (choice.equals("yes")) {
            result = true;
            check = "true";
        } else {
            result = false;
            System.out.printf("\"%s\" is not a valid choice.\n", choice);
        }

        return result;
    }
}

