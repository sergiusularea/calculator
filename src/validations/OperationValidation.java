package validations;
/**
 * This class is used to check if a string can be an operation
 * If true, it will be returned
 */
public class OperationValidation implements IValidation {
    @Override
    public boolean validateInput(String input) {
        boolean validate = false;
        if (input.matches("[+/^*-]")) {
            validate = true;
        } else {
            System.out.printf("\"%s\" is not a valid operation.\n", input);
        }
        return validate;
    }
}
